#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""Croot.
A simple REST receiver designed to receive trigger POST/DELETE messages.
"""

import logging
import os

from flask import Flask, abort, request

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

# Integrate with sentry?
SENTRY_DSN = os.environ.get('CROOT_SENTRY_DSN')
if SENTRY_DSN:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[FlaskIntegration()]
    )

# pylint: disable=invalid-name
app = Flask(__name__)
# Disable distracting logging...
log = logging.getLogger('werkzeug')
log.disabled = True
app.logger.disabled = True

_BANNER_WIDTH = 48


@app.route('/trigger', methods=['DELETE', 'POST'])
def trigger():
    """Handles trigger POST and DELETE messages.
    The payload is expected to be a JSON block, which can be
    a list of keyword/value pairs or a single set of keywords
    and values.
    """
    if not request.json:
        abort(400)

    # Beautify the message.
    #
    # Print a clear banner...
    banner_trail = _BANNER_WIDTH - len(request.method) - 1
    banner_trail_str = banner_trail * '-'
    print(' ')
    print(f'{request.method} {banner_trail_str}')
    # Do we have a list of json structures or just one?
    if isinstance(request.json, list):
        for json_block in request.json:
            _dump_json(json_block)
    else:
        _dump_json(request.json)

    return ''


def _dump_json(key_val_block):
    """Prints what is expected to be a JSON (dictionary) structure
    of keywords and values.
    """
    # ...and then format the payload
    # with neatly aligned keys and values.
    # Find the length of the longest key...
    max_key_length = 0
    for key in key_val_block:
        this_key_length = len(key)
        if this_key_length > max_key_length:
            max_key_length = this_key_length
    # Print...
    if max_key_length:
        for item in sorted(key_val_block.items()):
            padding = max_key_length - len(item[0])
            padding_str = padding * ' '
            print(f' {padding_str}{item[0]}: {item[1]}')
