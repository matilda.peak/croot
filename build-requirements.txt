# This are the requirements 'over and above' the run-time requirements
# and usually consist of Python stuff required to build and distribute.
# To fully configure the system you need to pass this and the
# runtime and package requirements to pip: -
#
# pip install -r build-requirements.txt
# pip install -r requirements.txt
# pip install -r package-requirements.txt

# Community modules...
pylint == 2.13.9
pytest == 7.1.2
