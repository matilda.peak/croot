---
Title:      The Patterneer Event Receiver
Author:     Alan Christie
Date:       13 May 2022
Copyright:  Matilda Peak. All rights reserved.
---

[![pipeline status](https://gitlab.com/matilda.peak/croot/badges/master/pipeline.svg)](https://gitlab.com/matilda.peak/croot/commits/master)

![Ansible Role](https://img.shields.io/ansible/role/42224)

# Croot
A simple Python (Flask-based) application that acts as a demonstration
endpoint (REST) for Patterneer. Pattern alarms can be sent to this
application, which simply logs (to stdout) the received information.

It completes the full end-to-end demonstration of Graten...

    [DATA] -> Garten -> Chronicler -> Patterneer -> Croot

## Container Building
There's a Dockerfile to build the Docker image. Main building is done
via the GitLab CI service (see `.gitlab-ci.yml`).

---
