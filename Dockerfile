# -----------------------------------------------------------------------------
# Copyright (C) 2022 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

ARG from_image=python:3.9.12-alpine3.15
FROM ${from_image}

# Labels
LABEL maintainer='Matilda Peak Limited <info@matildapeak.com>'

# Force the binary layer of the stdout and stderr streams
# to be unbuffered
ENV PYTHONUNBUFFERED 1

# Base directory for the application
# Also used for user directory
ENV APP_ROOT /home/croot

# Containers should NOT run as root
# (as good practice)
RUN adduser -D -h ${APP_ROOT} -s /bin/sh croot
RUN chown -R croot.croot ${APP_ROOT}

WORKDIR ${APP_ROOT}

COPY requirements.txt ./
RUN pip install -r ./requirements.txt

COPY croot.py ./
COPY wsgi.py ./
COPY LICENCE.txt ./

USER croot
ENV HOME ${APP_ROOT}

CMD gunicorn -w 1 --bind 0.0.0.0:8000 wsgi:app
