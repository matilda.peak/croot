import unittest

import json
import croot


class TestCroot(unittest.TestCase):

    def setUp(self):
        self.app = croot.app.test_client()

    def test_trigger_post_without_payload(self):
        response = self.app.post('/trigger')
        self.assertEqual(400, response.status_code)

    def test_trigger_post_with_payload(self):
        response = self.app.post('/trigger',
                                 data=json.dumps(dict({'a': 1})),
                                 content_type='application/json')
        self.assertEqual(200, response.status_code)

    def test_trigger_delete(self):
        response = self.app.delete('/trigger',
                                   data=json.dumps(dict({'a': 1})),
                                   content_type='application/json')
        self.assertEqual(200, response.status_code)

    def test_trigger_get(self):
        response = self.app.delete('/trigger')
        self.assertEqual(400, response.status_code)
